# frozen_string_literal: true

module TdeAuth
  class Auth
    def initialize(login, password)
      @login = login.strip
      @login = login[1..-1] if login[0] == "@"
      @password = password
    end

    def user
      internal_user = User.find_by_username_or_email(@login)
      return internal_user if internal_user&.confirm_password?(@password)

      external_data = Sso.auth(@login, @password)
      return unless external_data

      internal_user ||= create_internal_user
      internal_user.activate
      internal_user
    end

    private

    def create_internal_user
      username = UserNameSuggester.suggest(@login)
      User.create!(email: @login, username: username, password: SecureRandom.hex)
    end
  end
end
