# frozen_string_literal: true

module TdeAuth
  class Engine < ::Rails::Engine
    engine_name "TdeAuth".freeze
    isolate_namespace TdeAuth
  end
end
